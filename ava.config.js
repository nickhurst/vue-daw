export default {
  files: [
    'test/**/*.{js,ts}',
  ],
  sources: [
    '**/*.{js,ts}',
    '!dist/**/*',
    '!public/**/*',
    '!_templates/**/*',
  ],
  cache: false,
  concurrency: 5,
  failFast: false,
  tap: true,
  verbose: true,
  compileEnhancements: false,
  extensions: [
    'ts',
  ],
  require: [
    '@babel/register',
    'ts-node/register/transpile-only',
  ],
  babel: {
    extensions: ['js'],
  },
};
