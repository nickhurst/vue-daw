FROM node:10-alpine

WORKDIR /app
COPY package.json yarn.lock /app/

RUN apk add --no-cache bash git openssh && \
  yarn install --pure-lockfile --ignore-engines

COPY . /app/

CMD yarn serve
