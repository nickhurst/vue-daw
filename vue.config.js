module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.common.js',
      },
    },
  },

  publicPath: process.env.NODE_ENV === 'production'
    ? '/vue-daw/'
    : '/',

  lintOnSave: false,

  pluginOptions: {
    quasar: {
      treeShake: true
    }
  },

  transpileDependencies: [
    /[\\\/]node_modules[\\\/]quasar[\\\/]/
  ]
};
