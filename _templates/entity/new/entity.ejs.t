---
to: src/entities/<%= name %>.ts
---
<%_ Entity = h.changeCase.pascalCase(name.split('/').reverse()[0]) %>
import { Entity } from './entity';

export interface <%= Entity %> extends Entity {
}
