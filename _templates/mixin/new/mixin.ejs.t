---
to: src/mixins/<%= name %>.ts
---
import { ComponentOptions } from 'vue';

const mixin: ComponentOptions = {
};

export default mixin;
