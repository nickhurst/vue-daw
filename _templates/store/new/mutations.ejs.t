---
to: src/store/<%= name %>/mutations.ts
---
import { MutationTree } from 'vuex';
import { State } from './state';

export interface Mutations extends MutationTree<State> {
}

const mutations: Mutations = {
}

export default mutations;
