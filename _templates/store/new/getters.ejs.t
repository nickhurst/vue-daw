---
to: src/store/<%= name %>/getters.ts
---
import { GetterTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';

export interface Getters extends GetterTree<State, RootState> {
}

const getters: Getters = {
}

export default getters;
