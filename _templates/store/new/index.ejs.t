---
to: src/store/<%= name %>/index.ts
---
<%_ moduleName = h.inflection.camelize(name.split('/').reverse()[0].replace(/-/g, '_'), true) %>
import { Module } from 'vuex';
import { RootState } from '@/store';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state, { State } from './state';

const <%= moduleName %>: Module<State, RootState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default <%= moduleName %>;
export * from './state';
