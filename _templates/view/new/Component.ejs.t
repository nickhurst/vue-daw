---
to: src/views/<%= name %>.vue
---
<%_ component = name.split('/').reverse()[0] %> <%_ rootClass = h.changeCase.param(component) %>
<template>
  <div class="<%= rootClass %>">
  </div>
</template>

<script>
export default {
  name: '<%= component %>',
};
</script>

<style lang="scss">
@import "~@/assets/color";
@import "~@/assets/common";

.<%= rootClass %> {
}
</style>
