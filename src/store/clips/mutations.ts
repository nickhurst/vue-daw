import { lensPath, omit, set } from 'ramda';
import { MutationTree } from 'vuex';
import { State } from './state';
import { Clip } from '@/models/clip';

export interface Mutations extends MutationTree<State> {
  create(state: State, payload: { clip: Clip }): void;
  update(state: State, payload: { id: string, key: string, value: any }): void;
  delete(state: State, payload: { id: string }): void;
}

const mutations: Mutations = {
  create(state, { clip }) {
    state.index = { ...state.index, [clip.id]: clip };
  },
  update(state, { id, key, value }) {
    const lens = lensPath(key.split('.'));
    const clip = state.index[id];
    const updated = set(lens, value, clip);

    state.index = { ...state.index, [id]: updated };
  },
  delete(state, { id }) {
    state.index = omit([id], state.index);
  },
};

export default mutations;
