import { Clip } from '@/models/clip';

export interface State {
  index: { [id: string]: Clip };
}

const state: State = {
  index: {},
};

export default state;
