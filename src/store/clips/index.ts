
import { Module } from 'vuex';
import { RootState } from '@/store';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state, { State } from './state';

const clips: Module<State, RootState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default clips;
export * from './state';
