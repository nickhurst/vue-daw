import { ActionContext, ActionTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';
import { Clip } from '@/models/clip';
import { color } from '@/utils/color';
import { uuid } from '@/utils/uuid';

type Context = ActionContext<State, RootState>

const buildAudioClip: (id: string, start: number, length: number, trackId: string, color?: string) => Clip =
  (id, start, length, trackId, clipColor = color.purple.light()) => ({
    id,
    type: 'audio',
    start,
    length,
    name: 'Audio Clip',
    color: clipColor,
    notes: [],
    trackId,
  });

const buildMidiClip: (id: string, start: number, length: number, trackId: string, color?: string) => Clip =
  (id, start, length, trackId, clipColor = color.purple.light()) => ({
    id,
    type: 'midi',
    start,
    length,
    name: 'MIDI Clip',
    color: clipColor,
    notes: [],
    trackId,
  });

export interface Actions extends ActionTree<State, RootState> {
  create(context: Context, payload: Pick<Clip, 'start' | 'length' | 'trackId'>): Promise<Clip>;
  delete(context: Context, payload: { id: string }): Promise<string>;
}

const actions: Actions = {
  async create({ commit, rootState }, { start, length, trackId }) {
    const id = uuid();
    const track = rootState.tracks.index[trackId];
    const clipBuilder = track.type === 'audio' ? buildAudioClip : buildMidiClip;
    const clip = clipBuilder(id, start, length, trackId);
    const updateTrack = {
      id: trackId,
      key: 'clips',
      value: [...track.clips, id],
    };

    commit('create', { clip });
    commit('tracks/update', updateTrack, { root: true })

    return clip;
  },
  async delete({ commit, state, rootState }, { id }) {
    const { trackId } = state.index[id];
    const { clips: trackClips } = rootState.tracks.index[trackId];
    const updateTrack = {
      id: trackId,
      key: 'clips',
      value: trackClips.filter(cid => cid !== id),
    };

    commit('tracks/update', updateTrack, { root: true });
    commit('delete', { id });

    return id;
  },
};

export default actions;
