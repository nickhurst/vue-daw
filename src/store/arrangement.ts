import { map, pipe, range } from 'ramda';
import { Module } from 'vuex';

import { RootState } from './index';

export interface EditorItem { id: number, type: string }
export interface State {
  editorItem?: EditorItem;
  currentTrack?: number;
}

const arrangement: Module<State, RootState> = {
  namespaced: true,
  state: {
    editorItem: undefined,
    currentTrack: undefined,
  },
  getters: {
    currentTrack({ currentTrack }, _getters, rootState) {
      if (currentTrack) return rootState.tracks.index[currentTrack];
    },
  },
  mutations: {
    setEditorItem(state, { id, type }: EditorItem) {
      state.editorItem = { id, type };
    },
    setCurrentTrack(state, { id }) {
      state.currentTrack = id;
    },
  },
};

export default arrangement;
