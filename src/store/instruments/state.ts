import { InstrumentConfig } from '@/models/instrument';

export interface State {
  index: { [id: string]: InstrumentConfig };
}

const state: State = {
  index: {},
};

export default state;
