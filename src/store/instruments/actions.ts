import { ActionContext, ActionTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';
import { InstrumentConfig } from '@/models/instrument';
import { uuid } from '@/utils/uuid';

type Context = ActionContext<State, RootState>

export interface Actions extends ActionTree<State, RootState> {
  create(store: Context, payload: Omit<InstrumentConfig, 'id'>): Promise<InstrumentConfig>;
  delete(store: Context, payload: { id: string }): Promise<string>;
}

const actions: Actions = {
  async create({ commit }, attrs) {
    const id = uuid();
    const instrument = { id, ...attrs };

    commit('create', { instrument });
    commit('tracks/update', {
      id: instrument.trackId,
      key: 'instrumentId',
      value: id,
    }, { root: true });

    return instrument;
  },
  async delete({ commit, state }, { id }) {
    const { trackId } = state.index[id];

    commit('delete', { id });
    commit('tracks/update', { id: trackId, key: 'instrument' });

    return id;
  },
};

export default actions;
