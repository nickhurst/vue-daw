import { lensPath, omit, set } from 'ramda';
import { MutationTree } from 'vuex';
import { InstrumentConfig } from '@/models/instrument';
import { State } from './state';

export interface Mutations extends MutationTree<State> {
  create(state: State, payload: { instrument: InstrumentConfig }): void;
  update(state: State, payload: { id: string, key: string, value: any }): void;
  updateOption(state: State, payload: { id: string, key: string, value: any }): void;
  updateType(state: State, payload: { id: string, type?: string }): void;
  delete(state: State, payload: { id: string }): void;
}

const mutations: Mutations = {
  create(state, { instrument }) {
    state.index = { ...state.index, [instrument.id]: instrument };
  },
  update(state, { id, key, value }) {
    const lens = lensPath(key.split('.'));
    const instrument = state.index[id];
    const updated = set(lens, value, instrument);

    state.index = { ...state.index, [id]: updated };
  },
  updateOption(state, { id, key, value }) {
    const lens = lensPath(key.split('.'));
    const instrument = state.index[id];
    const options = set(lens, value, instrument.options);

    state.index = { ...state.index, [id]: { ...instrument, options } };
  },
  updateType(state, { id, type }) {
    const lens = lensPath(['type']);
    const instrument = state.index[id];
    const updated = set(lens, type, instrument);

    state.index = { ...state.index, [id]: updated };
  },
  delete(state, { id }) {
    state.index = omit([id], state.index);
  },
};

export default mutations;
