import Tone from 'tone';
import { GetterTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';

export interface Getters extends GetterTree<State, RootState> {
  time(state: State, getters: any, rootState: RootState, rootGetters: any): string;
  loopStart(state: State, getters: any, rootState: RootState, rootGetters: any): string;
  loopEnd(state: State, getters: any, rootState: RootState, rootGetters: any): string;
}

const getters: Getters = {
  time({ time }) {
    return new Tone.Time(time).toBarsBeatsSixteenths();
  },
  loopStart({ loopStart }) {
    return new Tone.Time(loopStart).toBarsBeatsSixteenths();
  },
  loopEnd({ loopEnd }) {
    return new Tone.Time(loopEnd).toBarsBeatsSixteenths();
  },
};

export default getters;
