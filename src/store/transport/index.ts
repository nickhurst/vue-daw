
import { Module } from 'vuex';
import { RootState } from '@/store';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state, { State } from './state';

const transport: Module<State, RootState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default transport;
export * from './state';
