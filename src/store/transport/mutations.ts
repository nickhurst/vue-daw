import Tone from 'tone';
import { MutationTree } from 'vuex';
import { State } from './state';

const toSeconds = (val: Tone.Encoding.Time) => new Tone.Time(val).toSeconds();

export interface Mutations extends MutationTree<State> {
  play(state: State): void;
  stop(state: State): void;
  setTime(state: State, payload: { time: Tone.Encoding.Time }): void;
  syncTime(state: State, payload: { time: number }): void;
  setTempo(state: State, payload: { tempo: number }): void;
  setSignature(state: State, payload: { signature: [number, number] }): void;
  loop(state: State, { loop }: { loop: boolean }): void;
  setLoopStart(state: State, payload: { start: Tone.Encoding.Time }): void;
  setLoopEnd(state: State, payload: { end: Tone.Encoding.Time }): void;
}

const mutations: Mutations = {
  play(state) {
    state.playing = true;
  },
  stop(state) {
    state.playing = false;
  },
  setTime(state, { time }) {
    state.time = toSeconds(time);
  },
  syncTime(state, { time }) {
    if (state.playing) state.time = time;
  },
  setTempo(state, { tempo }) {
    state.tempo = tempo;
  },
  setSignature(state, { signature }) {
    state.signature = signature;
  },
  loop(state, { loop }) {
    state.loop = loop;
  },
  setLoopStart(state, { start }) {
    state.loopStart = toSeconds(start);
  },
  setLoopEnd(state, { end }) {
    state.loopEnd = toSeconds(end);
  },
};

export default mutations;
