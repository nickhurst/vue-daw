import Tone from 'tone';

export interface State {
  time: number;
  tempo: number;
  playing: boolean;
  signature: [number, number];
  loop: boolean;
  loopStart: number;
  loopEnd: number;
}

const state: State = {
  time: new Tone.Time('0:0:0').toSeconds(),
  tempo: 128,
  signature: [4, 4],
  playing: false,
  loop: false,
  loopStart: new Tone.Time('0:0').toSeconds(),
  loopEnd: new Tone.Time('1:2').toSeconds(),
};

export default state;
