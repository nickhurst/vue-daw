import { ActionTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';

export interface Actions extends ActionTree<State, RootState> {
}

const actions: Actions = {
}

export default actions;
