import { EffectConfig } from '@/models/effect';

export interface State {
  index: { [id: string]: EffectConfig };
}

const state: State = {
  index: {},
};

export default state;
