import { lensPath, omit, set } from 'ramda';
import { MutationTree } from 'vuex';
import { State } from './state';
import { EffectConfig } from '@/models/effect';

export interface Mutations extends MutationTree<State> {
  create(state: State, payload: { effect: EffectConfig }): void;
  update(state: State, payload: { id: string, key: string, value: any }): void;
  enable(state: State, payload: { id: string }): void;
  disable(state: State, payload: { id: string }): void;
  delete(state: State, payload: { id: string }): void;
}

const mutations: Mutations = {
  create(state, { effect }) {
    state.index = {
      ...state.index,
      [effect.id]: { ...effect, enabled: true },
    };
  },
  update(state, { id, key, value }) {
    const lens = lensPath(key.split('.'));
    const clip = state.index[id];
    const updated = set(lens, value, clip);

    state.index = { ...state.index, [id]: updated };
  },
  enable(state, { id }) {
    const effect = state.index[id];

    state.index = {
      ...state.index,
      [id]: {
        ...effect,
        enabled: true,
      },
    };
  },
  disable(state, { id }) {
    const effect = state.index[id];

    state.index = {
      ...state.index,
      [id]: {
        ...effect,
        enabled: false,
      },
    };
  },
  delete(state, { id }) {
    state.index = omit([id], state.index);
  },
}

export default mutations;
