import { ActionContext, ActionTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';
import { EffectConfig } from '@/models/effect';
import { uuid } from '@/utils/uuid';

type Context = ActionContext<State, RootState>

export interface Actions extends ActionTree<State, RootState> {
  create(store: Context, payload: Omit<EffectConfig, 'id'>): Promise<EffectConfig>;
  delete(store: Context, payload: { id: string }): Promise<string>;
}

const actions: Actions = {
  async create({ commit, rootState: { tracks } }, { trackId, ...config }) {
    const id = uuid();
    const effect = { id, trackId, ...config };
    const { effects } = tracks.index[trackId];

    commit('create', { effect });
    commit('tracks/update', {
      id: trackId,
      key: 'effects',
      value: [...effects, id],
    }, { root: true });

    return { id, trackId, ...config };
  },
  async delete({ commit, state, rootState: { tracks } }, { id }) {
    const { trackId } = state.index[id];
    const { effects } = tracks.index[trackId];

    commit('delete', { id });
    commit('tracks/update', {
      id: trackId,
      key: 'effects',
      value: effects.filter(eid => eid !== id),
    }, { root: true });

    return id;
  },
};

export default actions;
