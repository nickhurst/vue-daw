import { Module } from 'vuex';
import { RootState } from '../';

import getters from './getters';
import mutations from './mutations';
import state, { State } from './state';

import editor from './editor';
import notes from './notes';

export default {
  namespaced: true,
  modules: {
    editor,
    notes,
  },
  state,
  getters,
  mutations,
} as Module<State, RootState>;

export * from './state';
