import Tone from 'tone';
import { MidiNote } from '@/models/midi-note';

export interface State {
  index: { [id: string]: MidiNote };
}

export default {
  index: {},
};
