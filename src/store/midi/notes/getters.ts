import Tone from 'tone';
import { range, take } from 'ramda';
import { GetterTree } from 'vuex';
import { Clip } from '@/models/clip';
import { State } from './state';
import { RootState } from '../../';

const getters: GetterTree<State, RootState> = {
  currentClipNotes({ index }, _getters, { arrangement: { editorItem }, clips }) {
    if (!editorItem || editorItem.type !== 'clip') return [];

    const clip = clips.index[editorItem.id] as Clip;
    if (!clip) return [];

    return clip.notes.map(id => index[id]);
  },
};

export default getters;
