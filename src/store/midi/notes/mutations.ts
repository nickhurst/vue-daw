import { lensPath, omit, set } from 'ramda';
import { MutationTree } from 'vuex';
import { State } from './state';
import { MidiNote } from '@/models/midi-note';
import { uuid } from '@/utils/uuid';

interface CreateNotePayload {
  note: MidiNote;
}

interface UpdateNotePayload {
  id: string;
  key: keyof Omit<MidiNote, 'id'>;
  value: MidiNote[keyof Omit<MidiNote, 'id'>];
}

const mutations: MutationTree<State> = {
  create(state, { note }: CreateNotePayload): void {
    state.index = { ...state.index, [note.id]: { ...note } };
  },
  update(state, { id, key, value }: UpdateNotePayload): void {
    const lens = lensPath(key.split('.'));
    const note = state.index[id];
    const updated = set(lens, value, note);

    state.index = { ...state.index, [id]: updated };
  },
  delete(state, { id }: { id: string }): void {
    state.index = omit([id], state.index);
  },
};

export default mutations;
