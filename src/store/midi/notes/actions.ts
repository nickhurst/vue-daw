import { ActionTree } from 'vuex';
import { State } from './state';
import { RootState } from '../..'
import { Clip } from '@/models/clip';
import { MidiNote } from '@/models/midi-note';
import { uuid } from '@/utils/uuid';

interface Attrs extends Omit<MidiNote, 'id'> {
  clipId: string;
}

const actions: ActionTree<State, RootState> = {
  async create({ commit, rootState }, attrs: Attrs): Promise<MidiNote> {
    const id = uuid();
    const clip = rootState.clips.index[attrs.clipId] as Clip;
    const note = { id, ...attrs };
    const updateClip = {
      id: attrs.clipId,
      key: 'notes',
      value: [...clip.notes, id],
    };

    commit('create', { note });
    commit('clips/update', updateClip, { root: true });

    return note;
  },
  async delete({ commit, state, rootState }, { id }: { id: string }): Promise<string> {
    const note = state.index[id];
    const clip = rootState.clips.index[note.clipId] as Clip;
    const updateClip = {
      id: clip.id,
      key: 'notes',
      value: clip.notes.filter(cid => cid !== id),
    };

    commit('clips/update', updateClip, { root: true });
    commit('delete', { id });

    return id;
  },
};

export default actions;
