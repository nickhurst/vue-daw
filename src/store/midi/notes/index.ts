import { Module } from 'vuex';
import { RootState } from '../../';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state, { State } from './state';

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
} as Module<State, RootState>;

export * from './state';
