import { GetterTree } from 'vuex';
import { State } from './state';
import { RootState } from '../';

const getters: GetterTree<State, RootState> = {};

export default getters;
