import { lensPath, set } from 'ramda';
import { MutationTree } from 'vuex';
import { State } from './state';

interface SetSelectedNotePayload {
  id?: string;
}

const mutations: MutationTree<State> = {
  setSelectedNote(state, { id }: SetSelectedNotePayload): void {
    state.selectedNote = id;
  },
};

export default mutations;
