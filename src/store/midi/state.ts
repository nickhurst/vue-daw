import { State as EditorState } from './editor';
import { State as NotesState } from './notes';

export interface State {
  selectedNote?: string;
  editor?: EditorState;
  notes?: NotesState;
}

export default {
  selectedNote: undefined,
};
