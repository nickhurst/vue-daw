import { getNoteIndex } from '@/models/note';

const DEFAULT_GRID_WIDTH = 1500;
const DEFAULT_GRID_HEIGHT = 400;
const DEFAULT_GRID_CELLS_X = 30;
const DEFAULT_GRID_CELLS_Y = 20;
const DEFAULT_GRID_TIMESCALE = '8n';
const DEFAULT_KEY_WIDTH = 75;
const DEFAULT_KEY_LABEL_WIDTH = 30;
const DEFAULT_KEY_START_INDEX = Math.round(Math.max(getNoteIndex('C', 4) - DEFAULT_GRID_CELLS_Y / 2, 0));
const TIMESCALES = [1, 2, 4, 8, 36].map(i => `${i}n`);

export interface State {
  gridWidth: number;
  gridHeight: number;
  gridCellsX: number;
  gridCellsY: number;
  gridTimescale: Notation;
  gridTimescales: ReadonlyArray<Notation>;
  keyWidth: number;
  keyLabelWidth: number;
  keyStartIndex: number;
  selectedNote?: string;
}

export default {
  gridWidth: DEFAULT_GRID_WIDTH,
  gridHeight: DEFAULT_GRID_HEIGHT,
  gridCellsX: DEFAULT_GRID_CELLS_X,
  gridCellsY: DEFAULT_GRID_CELLS_Y,
  gridTimescale: DEFAULT_GRID_TIMESCALE,
  gridTimescales: TIMESCALES,
  keyWidth: DEFAULT_KEY_WIDTH,
  keyLabelWidth: DEFAULT_KEY_LABEL_WIDTH,
  keyStartIndex: DEFAULT_KEY_START_INDEX,
  selectedNote: undefined,
};
