import { lensPath, set } from 'ramda';
import { MutationTree } from 'vuex';
import { State } from './state';
import { NOTES } from '@/utils/constants/notes';
import { wheelEventYDirection, WheelDirection } from '@/utils/dom';
import { uuid } from '@/utils/uuid';

interface ScrollVisibleNotesPayload {
  wheelEvent: WheelEvent;
}

export interface Mutations extends MutationTree<State> {
  setGridDimensions(state: State, payload: { height: number, width: number }): void;
  setGridTimescale(state: State, payload: { scale: Notation }): void;
  setSelectedNote(state: State, payload: { id?: string }): void;
  scrollVisibleNotes(state: State, payload: { wheelEvent: WheelEvent }): void;
}

const mutations: Mutations = {
  setGridDimensions(state, { height, width }) {
    state.gridHeight = height;
    state.gridWidth = width;
  },
  setGridTimescale(state, { scale }) {
    state.gridTimescale = scale;
  },
  setSelectedNote(state, { id }) {
    state.selectedNote = id;
  },
  scrollVisibleNotes(state, { wheelEvent }) {
    const current = state.keyStartIndex;
    const direction = wheelEventYDirection(wheelEvent);

    if (direction === WheelDirection.Up) {
      state.keyStartIndex = Math.max(current - 1, 0);
    } else if (direction === WheelDirection.Down) {
      state.keyStartIndex = Math.min(current + 1, NOTES.length);
    }
  },
};

export default mutations;
