import Tone from 'tone';
import { head, last, range, take, slice } from 'ramda';
import { GetterTree } from 'vuex';
import { State } from './state';
import { RootState } from '../../';
import { NOTES, NOTE_LETTERS, Note, NoteLetter } from '@/models/note';
import { MidiNote } from '@/models/midi-note';

interface ClipCell {
  num: number;
}

interface Getters {
  clipCells: ReadonlyArray<ClipCell>;
  gridCellXOffset: number;
  gridCellWidth: number;
  gridCellHeight: number;
  gridCellTimeValue: number;
  gridClipCells: ReadonlyArray<ClipCell>;
  gridKeyNotes: ReadonlyArray<Note>;
  gridMidiNotes: ReadonlyArray<MidiNote>;
}

interface GetterFns extends GetterFunctions<Getters, State, RootState>, GetterTree<State, RootState> {}

const noteToSeconds = (str: string) => new Tone.Time(str).toSeconds();

const getters: GetterFns = {
  clipCells({ gridTimescale }, _getters, { arrangement: { editorItem }, clips }) {
    if (!editorItem || editorItem.type !== 'clip') return [];

    const clip = clips.index[editorItem.id];
    const scale = parseInt(gridTimescale.replace(/[A-z]/g, ''))
    const cells = scale * clip.length;

    return range(0, cells).map(num => ({ num })) as ReadonlyArray<{ num: number }>;
  },
  gridCellXOffset({ keyWidth, keyLabelWidth }) {
    return keyWidth + keyLabelWidth;
  },
  gridCellWidth({ gridWidth, gridCellsX }) {
    return gridWidth / gridCellsX;
  },
  gridCellHeight({ gridHeight, gridCellsY }) {
    return gridHeight / gridCellsY;
  },
  gridCellTimeValue({ gridTimescale }) {
    return noteToSeconds(gridTimescale);
  },
  gridClipCells({ gridCellsX }, { clipCells }) {
    return take(gridCellsX, clipCells);
  },
  gridKeyNotes({ keyStartIndex, gridCellsY }) {
    const total = NOTES.length;
    const fromIndex = Math.max(0, keyStartIndex);
    const toIndex = Math.min((fromIndex + gridCellsY - 1), total);

    return slice(fromIndex, toIndex, NOTES);
  },
  gridMidiNotes(_state, { gridKeyNotes }, _rootState, rootGetters) {
    const midiNotes: ReadonlyArray<MidiNote> =
      rootGetters['midi/notes/currentClipNotes'];

    const maxNote = head(gridKeyNotes);
    const minNote = last(gridKeyNotes);

    return midiNotes.filter(({ note: { letter, pitch } }) => {
      const index = NOTE_LETTERS.indexOf(letter);
      const minIndex = NOTE_LETTERS.indexOf(minNote.letter);
      const maxIndex = NOTE_LETTERS.indexOf(maxNote.letter);

      if (minNote.pitch > pitch) return false;
      if (minNote.pitch === pitch && index < minIndex) return false;
      if (maxNote.pitch < pitch) return false;
      if (maxNote.pitch === pitch && index > maxIndex) return false;

      return true;
    });
  },
};

export default getters;
