
import { Module } from 'vuex';
import { RootState } from '@/store';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state, { State } from './state';

const tracks: Module<State, RootState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default tracks;
export * from './state';
