import { values } from 'ramda';
import { GetterTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';
import { TrackConfig } from '@/models/track';

export interface Getters extends GetterTree<State, RootState> {
  tracks(state: State, getters: any, rootState: RootState, rootGetters: any): ReadonlyArray<TrackConfig>;
}

const getters: Getters = {
  tracks({ index }) {
    return values(index);
  },
};

export default getters;
