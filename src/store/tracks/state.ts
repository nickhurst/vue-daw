import { TrackConfig } from '@/models/track';

export interface State {
  index: { [id: string]: TrackConfig };
}

const state: State = {
  index: {},
};

export default state;
