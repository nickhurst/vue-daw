import { ActionContext, ActionTree } from 'vuex';
import { State } from './state';
import { RootState } from '@/store';
import { TrackConfig } from '@/models/track';
import { color } from '@/utils/color';
import { uuid } from '@/utils/uuid';

type Context = ActionContext<State, RootState>

export interface Actions extends ActionTree<State, RootState> {
  create(context: Context, payload: Omit<TrackConfig, 'id' | 'effects' | 'instrumentId'>): Promise<TrackConfig>;
  delete(context: Context, payload: { id: string }): Promise<string>;
}

const TRACK_DEFAULTS = {
  type: 'midi',
  name: 'MIDI Track 1',
  instrument: undefined,
  clips: [],
  effects: [],
  settings: {},
}

const actions: Actions = {
  async create({ commit }, attrs) {
    const id = uuid();
    const track = { ...TRACK_DEFAULTS,...attrs, id };

    commit('create', { track });

    return track as TrackConfig;
  },
  async delete({ commit }, { id }) {
    commit('delete', { id });

    return id;
  },
}

export default actions;
