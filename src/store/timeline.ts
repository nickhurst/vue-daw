import { Module } from 'vuex';

import { RootState } from './index';

interface TimelineNode { id: number; track: number };
interface SelectRange { start: number; end?: number; }

interface State {
  zoom: number;
  nodes: number;
  cursor: number;
  lastEnteredNode?: TimelineNode;
  selectRange?: SelectRange;
}

const timeline: Module<State, RootState> = {
  namespaced: true,
  state: {
    zoom: 1,
    nodes: 48,
    cursor: 0,
    lastEnteredNode: undefined,
    selectRange: undefined,
  },
  getters: {
    nodeWidth: state => (1645 / state.nodes),
    selection: ({ selectRange, lastEnteredNode }): SelectRange | undefined => {
      if (!selectRange || !lastEnteredNode) return;
      return {
        start: selectRange.start,
        end: selectRange.end || lastEnteredNode.id,
      };
    },
  },
  mutations: {
    startSelecting(state, { id }: { id: number }) {
      state.selectRange = {
        start: id,
        end: undefined,
      };
    },
    setSelection(state) {
      if (!state.selectRange || !state.lastEnteredNode) return;
      state.selectRange = {
        ...state.selectRange,
        end: state.lastEnteredNode.id,
      };
    },
    clearSelection(state) {
      state.selectRange = undefined;
    },
    setEnteredNode(state, { id, track }: TimelineNode) {
      state.lastEnteredNode = { id, track };
    },
  },
};

export { State as TimelineState };
export default timeline;
