import Vue, { VueConstructor } from 'vue';
import Vuex, { Store as _Store } from 'vuex';

import daw from '@/daw/vuex';

import arrangement, { State as ArrangementState } from './arrangement';
import clips, { State as ClipsState } from './clips/index';
import effects, { State as EffectsState } from './effects';
import instruments, { State as InstrumentsState } from './instruments';
import midi, { State as MidiState } from './midi';
import timeline, { TimelineState } from './timeline';
import tracks, { State as TracksState } from './tracks/index';
import transport, { State as TransportState } from './transport';

Vue.use(Vuex);

interface ContextMenuOptions {
  position: XYCoordObject;
  component: VueConstructor;
  props?: object;
}

interface State {
  contextMenu?: ContextMenuOptions;
}

export interface RootState extends State {
  arrangement: ArrangementState;
  clips: ClipsState;
  effects: EffectsState;
  instruments: InstrumentsState;
  midi: MidiState;
  timeline: TimelineState;
  tracks: TracksState;
  transport: TransportState;
};

export type Store = _Store<RootState>;

export default new Vuex.Store<State>({
  plugins: [daw],
  modules: {
    arrangement,
    clips,
    effects,
    instruments,
    midi,
    timeline,
    tracks,
    transport,
  },
  state: {
    contextMenu: undefined,
  },
  mutations: {
    openContextMenu(state, { menu }: { menu: ContextMenuOptions }) {
      state.contextMenu = menu;
    },
    closeContextMenu(state) {
      state.contextMenu = undefined;
    },
  },
});
