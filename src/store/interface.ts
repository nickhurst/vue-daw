import { VueConstructor } from 'vue';
import { Module } from 'vuex';

import { RootState } from './index';

interface ContextMenuOptions {
  position: XYCoordObject;
  component: VueConstructor;
  props?: object;
}

interface State { contextMenu?: ContextMenuOptions }

const iface: Module<State, RootState> = {
  namespaced: true,
  state: {
    contextMenu: undefined,
  },
  mutations: {
    openContextMenu(state, { menu }: { menu: ContextMenuOptions }) {
      state.contextMenu = menu;
    },
    closeContextMenu(state) {
      state.contextMenu = undefined;
    },
  },
};

export { State as InterfaceState };
export default iface;
