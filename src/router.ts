import Vue from 'vue';
import Router from 'vue-router';
import Arrangement from './views/Arrangement.vue';
import About from './views/About.vue';

Vue.use(Router);

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'arrangement',
      component: Arrangement,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
  ],
});
