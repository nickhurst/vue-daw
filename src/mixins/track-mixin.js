import { keys } from 'ramda';
import { mapMutations, mapState } from 'vuex';

export default {
  props: {
    trackId: {
      type: String,
      required: true,
    },
  },
  computed: {
    track() {
      const track = this.tracksIndex[this.trackId];
      const clips = track.clips.reduce((arr, id) => {
        const clip = this.clipsIndex[id];
        const start = clip.start;
        const end = (clip.start + 1) + clip.length;

        return [...arr, { id, start, end }];
      }, []);

      return { ...track, clips };
    },
    ...mapState('clips', { clipsIndex: 'index' }),
    ...mapState('tracks', { tracksIndex: 'index' }),
  },
  methods: {
    ...mapMutations('tracks', ['updateTrack']),
  },
};
