import Tone from 'tone';
import { DAWObject } from '@/daw/daw-object';
import { Factory, Registry } from '@/daw/registry';
import { Store } from '@/daw/store';
import { Instrument } from './instrument';

export interface TrackOptions {
  id: string;
  type?: 'audio' | 'midi';
  volume?: number;
  pan?: number;
}

export interface TrackConfig extends TrackOptions {
  name: string;
  mute: boolean;
  solo: boolean;
  clips: string[]
  effects: string[]
  instrumentId?: string;
}

export class Track extends DAWObject {
  private _type: 'audio' | 'midi';
  private _instrumentId: string | undefined;
  private _effects: string[];
  private _volume: Tone.Volume;
  private _panner: Tone.Panner;
  private _solo: Tone.Solo;

  constructor(store: Store, _registry: Registry, { id, type, volume = 0, pan = 0 }: TrackOptions) {
    super(store, id);

    this._type = type || 'midi';
    this._volume = new Tone.Volume({ volume, mute: false });
    this._panner = new Tone.Panner(pan);
    this._solo = new Tone.Solo();
    this._effects = [];
  }

  get type() {
    return this._type;
  }

  get instrumentId() {
    return this._instrumentId;
  }

  set instrumentId(value: string | undefined) {
    this._instrumentId = value;
    this.buildEffectsChain();
  }

  get instrument(): Instrument {
    return this.store.getInstrument(this._instrumentId) as Instrument;
  }

  get volume() {
    return this._volume.volume.value;
  }

  set volume(value: number) {
    this._volume.volume.value = value;
  }

  get mute() {
    return this._volume.mute;
  }

  set mute(value: boolean) {
    this._volume.mute = value;
  }

  get pan() {
    return this._panner.pan.value;
  }

  set pan(value: number) {
    this._panner.pan.value = value;
  }

  get solo() {
    return this._solo.solo;
  }

  set solo(value: boolean) {
    this._solo.solo = value;
  }

  get effects() {
    return this._effects;
  }

  set effects(value: string[]) {
    this._effects = value;
    this.buildEffectsChain();
  }

  public set(keyPath: string, value: any): void {
    const [target, key] = this.parseKeyPath(keyPath);
    target[key] = value;
  }

  public buildEffectsChain() {
    const nodes = [...this.effectNodes, ...this.defaultEffects];
    this.chainEffects(...nodes);
  }

  private chainEffects(...nodes: Tone.ProcessingNode[]): void {
    if (!this.instrument || !this.instrument.node) return;
    this.instrument.node.chain(...nodes);
  }

  private get effectNodes() {
    const nodes = this.effects
      .map(id => this.store.getEffect(id))
      .filter(eff => eff && eff.enabled)
      .map(eff => eff.node)
      .filter(node => node && node['_context']);

    return nodes;
  }

  private get defaultEffects() {
    const master = this.getMaster();
    return [this._panner, this._solo, this._volume, master];
  }
}
