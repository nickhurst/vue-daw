import { range, xprod, zipObj } from 'ramda';

export type NotePitch = 1 | 2 | 3 | 4 | 5 | 6 | 7;
export type NoteLetter = 'C' | 'C#' | 'D' | 'D#' | 'E' | 'F' | 'F#' | 'G' | 'G#' | 'A' | 'A#' | 'B';
export type NotePair = [NoteLetter, NotePitch];

export interface Note {
  letter: NoteLetter;
  pitch: NotePitch;
}

const sortNotes: (a: NotePair, b: NotePair) => number =
  ([noteA, pitchA], [noteB, pitchB]) => {
    if (pitchA !== pitchB) return pitchB - pitchA;
    return NOTE_LETTERS.indexOf(noteB) - NOTE_LETTERS.indexOf(noteA);
  };

const toNoteObj: (pair: NotePair) => Note =
  zipObj(['letter', 'pitch']) as any;

export const NOTE_PITCHES: ReadonlyArray<NotePitch> =
  range(1, 7) as ReadonlyArray<NotePitch>;

export const NOTE_LETTERS: ReadonlyArray<NoteLetter> = [
  'C',
  'C#',
  'D',
  'D#',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'A',
  'A#',
  'B',
];

export const NOTES: ReadonlyArray<Note> =
  xprod(NOTE_LETTERS, NOTE_PITCHES)
    .sort(sortNotes)
    .map(toNoteObj);

export const getNoteIndex: (letter: NoteLetter, pitch: NotePitch, notes?: ReadonlyArray<Note>) => number =
  (letter, pitch, notes = NOTES) =>
    notes.findIndex(note => note.letter === letter && note.pitch === pitch);
