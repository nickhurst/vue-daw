export type ClipType = 'midi' | 'audio';

export interface Clip {
  id: string;
  type: ClipType;
  notes: string[];
  start: number;
  length: number;
  name?: string;
  color?: string;
  trackId: string;
}
