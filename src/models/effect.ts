import Tone from 'tone';
import { DAWObject } from '@/daw/daw-object';
import { Factory, Registry } from '@/daw/registry';
import { Store } from '@/daw/store';
import { Track } from './track';
import { isPromise } from '@/utils/predicates';

export interface EffectConfig {
  id: string;
  type: string;
  trackId: string;
  enabled: boolean;
  options?: any;
}

export class Effect extends DAWObject {
  private _type: string;
  private _factory: Factory;
  private _trackId: string;
  private _enabled: boolean;
  private _options: any;
  private _node: Tone.AudioNode;
  private readonly _registry: Registry;

  constructor(store: Store, registry: Registry, config: EffectConfig) {
    const { id, type, trackId, enabled = true, options } = config;

    super(store, id);

    this._type = type;
    this._factory = registry.effects.get(this.type)
    this._trackId = trackId;
    this._enabled = enabled;
    this._options = options;
    this._registry = registry;

    this.initializeNode(options);
  }

  get type() {
    return this._type;
  }

  get node() {
    return this._node;
  }

  get track() {
    return this.store.getTrack(this._trackId);
  }

  get enabled() {
    return this._enabled;
  }

  set(keyPath: string, value: any): void {
    keyPath = keyPath.replace(/^options\./, '');
    const [target, key] = this.parseKeyPath(keyPath, this.node);

    if (this.enabled) {
      if (this._factory.metadata.signals.includes(key)) {
        this.setSignalValue(target, key, value);
      } else {
        target[key] = value;
      }
    }

    this._options[key] = value;
  }

  async enable(): Promise<void> {
    if (this.enabled) return;

    await this.initializeNode(this._options);
    this._enabled = true

    Object.keys(this._options)
      .forEach(key => this.set(key, this._options[key]));

    this.track.buildEffectsChain();
  }

  disable(): void {
    if (!this.enabled) return;

    this.node.dispose();
    this._node = null;
    this._enabled = false;

    this.track.buildEffectsChain();
  }

  private async initializeNode(options: any) {
    const node = this._factory.createInstance(options);

    if (isPromise(node)) this._node = await node;
    else this._node = node;
  }
};
