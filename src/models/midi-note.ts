import { Note } from './note';

export interface MidiNote {
  id: string;
  note: Note;
  start: number;
  duration: number;
  clipId?: string;
}
