import Tone from 'tone';
import { DAWObject } from '@/daw/daw-object';
import { Factory, Registry } from '@/daw/registry';
import { Store } from '@/daw/store';

export interface InstrumentConfig {
  id: string;
  type: string;
  trackId: string;
  poly?: boolean;
  polyVoices?: number;
  options?: any;
}

export class Instrument extends DAWObject {
  private _type: string;
  private _factory: Factory;
  private _node: Tone.Instrument;
  private _poly: boolean;
  private _polyVoices: number;
  private _trackId: string;

  constructor(store: Store, registry: Registry, config: InstrumentConfig) {
    const { id, type, poly, polyVoices, options, trackId } = config;

    super(store, id)

    this._type = type;
    this._poly = poly;
    this._polyVoices = polyVoices;
    this._trackId = trackId;
    this._factory = registry.instruments.get(type);

    this.initializeNode(options);
  }

  get type() {
    return this._type;
  }

  get node() {
    return this._node;
  }

  get track() {
    return this.store.getTrack(this._trackId);
  }

  get poly() {
    return this._poly;
  }

  set poly(value: boolean) {
    this._poly = value;
    this.recreateInstrument();
  }

  get polyVoices() {
    return this._polyVoices;
  }

  set polyVoices(value: number) {
    if (this.poly) {
      this._polyVoices = Math.max(1, value);
      this.recreateInstrument();
    }
  }

  public set(keyPath: string, value: any): void {
    if (['type', 'poly', 'polyVoices'].includes(keyPath)) {
      return this[keyPath] = value;
    }

    if (this.poly) {
      const node = this.node as Tone.PolySynth;
      node.set(keyPath, value);

      return;
    }

    keyPath = keyPath.replace(/^options\./, '');
    const [target, key] = this.parseKeyPath(keyPath, this.node);

    if (this._factory.metadata.signals.includes(key)) {
      this.setSignalValue(target, key, value);
    } else {
      target[key] = value;
    }
  }

  public triggerAttackRelease(note: string, duration: number, time: number) {
    this.node.triggerAttackRelease(note, duration, time);
    return this;
  }

  private recreateInstrument() {
    this._node = this.initializeNode({});
    if (this.track) this.track.buildEffectsChain();
  }

  private initializeNode(options: any): Tone.Instrument {
    const poly = this.poly;
    this._node = this._factory.createInstance(options, { poly });

    return this._node;
  }
}
