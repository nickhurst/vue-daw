import Tone from 'tone';
import { State } from '@/store/transport';

export class Transport {
  private readonly transport: Tone.Transport;

  constructor() {
    this.transport = Tone.Transport;
  }

  get state() {
    return this.transport.state;
  }

  get time() {
    return this.transport.seconds;
  }

  set time(value: Tone.Encoding.Time) {
    this.transport.seconds = new Tone.Time(value).toSeconds();
  }

  get loop() {
    return this.transport.loop;
  }

  set loop(value: boolean) {
    this.transport.loop = value;
  }

  get loopStart() {
    return new Tone.Time(this.transport.loopStart).toBarsBeatsSixteenths();
  }

  set loopStart(value: Tone.Encoding.Time) {
    this.transport.loopEnd = new Tone.Time(value).toSeconds();
  }

  get loopEnd() {
    return new Tone.Time(this.transport.loopEnd).toBarsBeatsSixteenths();
  }

  set loopEnd(value: Tone.Encoding.Time) {
    this.transport.loopEnd = new Tone.Time(value).toSeconds();
  }

  get signature() {
    return this.transport.timeSignature;
  }

  set signature(value: number | [number, number]) {
    this.transport.timeSignature = value;
  }

  get bpm() {
    return this.transport.bpm.value;
  }

  set bpm(value: number) {
    this.transport.bpm.value = value;
  }

  public start(time: Tone.Encoding.Time) {
    if (this.state === 'started') return;

    this.transport.start('+0.1', time);
  }

  public stop() {
    if (this.state === 'stopped') return;

    this.transport.stop();
  }

  public syncState({ time, tempo, signature, loop, loopStart, loopEnd }: State) {
    this.time = time;
    this.bpm = tempo;
    this.signature = signature;
    this.loop = loop;
    this.loopStart = loopStart;
    this.loopEnd = loopEnd;
  }

  public onTick(callback: (transport: Tone.Transport) => void, ticks = 1) {
    const drawEvent = (time: number) =>
      Tone.Draw.schedule(() => callback(this.transport), time)

    return this.transport.scheduleRepeat(drawEvent, `${ticks}i`);
  }
}
