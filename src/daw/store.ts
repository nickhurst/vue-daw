import { Effect } from '@/models/effect';
import { Instrument } from '@/models/instrument';
import { Track } from '@/models/track';

type Index<T> = { [key: string]: T };

export class Store {
  readonly effects: Index<Effect>;
  readonly instruments: Index<Instrument>;
  readonly tracks: Index<Track>;

  constructor() {
    this.effects = {};
    this.instruments = {};
    this.tracks = {};
  }

  public addEffect(effect: Effect): void {
    this.effects[effect.id] = effect;
  }

  public addInstrument(instrument: Instrument): void {
    this.instruments[instrument.id] = instrument;
  }

  public addTrack(track: Track): void {
    this.tracks[track.id] = track;
  }

  public getEffect(id: string): Effect {
    return this.effects[id];
  }

  public getInstrument(id: string): Instrument {
    return this.instruments[id];
  }

  public getTrack(id: string): Track {
    return this.tracks[id];
  }
}
