import Tone from 'tone';
import daw from './index';
import { Clip } from '@/models/clip';
import { MidiNote } from '@/models/midi-note';
import { RootState } from '@/store';
import { Plugin } from 'vuex';

const createEffect = (_state, { effect }) =>
  daw.createEffect(effect);

const updateEffect = (_state, { id, key, value }) =>
  daw.store.getEffect(id).set(key, value);

const enableEffect = (_state, { id, key, value }) =>
  daw.store.getEffect(id).enable();

const disableEffect = (_state, { id, key, value }) =>
  daw.store.getEffect(id).disable();

const createInstrument = (_state, { instrument }) =>
  daw.createInstrument(instrument)

const updateInstrument = (_state, { id, key, value }) =>
  daw.store.getInstrument(id).set(key, value);

const updateInstrumentOption = (_state, { id, key, value }) =>
  daw.store.getInstrument(id).set(key, value);

const createTrack = (_state, { track }) =>
  daw.createTrack(track);

const updateTrack = (_state, { id, key, value }) =>
  daw.store.getTrack(id).set(key, value);

const scheduleMidiNote = (state: RootState, { note }: { note: MidiNote }) => {
  const clip = state.clips.index[note.clipId];
  if (clip.type === 'midi') {
    const { instrumentId } = state.tracks.index[clip.trackId];

    daw.scheduler.scheduleMidiClipNote(instrumentId, clip, note);
  }
}

const updateMidiNote = (state: RootState, { id }: { id: string }) => {
  const note = state.midi.notes.index[id];
  const clip = state.clips.index[note.clipId];
  if (clip.type === 'midi') {
    const { instrumentId } = state.tracks.index[clip.trackId];

    daw.scheduler.scheduleMidiClipNote(instrumentId, clip, note);
  }
}

const mutationHandler = {
  'effects/create': createEffect,
  'effects/update': updateEffect,
  'effects/enable': enableEffect,
  'effects/disable': disableEffect,
  'instruments/create': createInstrument,
  'instruments/update': updateInstrument,
  'instruments/updateOption': updateInstrumentOption,
  'midi/notes/create': scheduleMidiNote,
  'midi/notes/update': updateMidiNote,
  'tracks/create': createTrack,
  'tracks/update': updateTrack,
  'transport/play': (state) => daw.transport.start(state.transport.time),
  'transport/stop': () => daw.transport.stop(),
  'transport/loop': (_state, { loop }) => daw.transport.loop = loop,
  'transport/setTime': (_state, { time }) => daw.transport.time = time,
  'transport/setTempo': (_state, { tempo }) => daw.transport.bpm = tempo,
};

const plugin: Plugin<RootState> = store => {
  daw.transport.syncState(store.state.transport);

  daw.transport.onTick(({ seconds }) =>
    store.commit('transport/syncTime', { time: seconds }));

  store.subscribe(({ type, payload }, state) => {
    const handler = mutationHandler[type];
    if (handler) handler(state, payload);
  });
};

export default plugin;
