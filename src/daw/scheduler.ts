import Tone from 'tone';
import { Clip } from '@/models/clip';
import { MidiNote } from '@/models/midi-note';
import { Store } from './store';

interface Scheduled {
  [id: string]: number;
}

export class Scheduler {
  private scheduled: Scheduled;
  private readonly store: Store;

  constructor(store: Store) {
    this.scheduled = {};
    this.store = store;
  }

  public scheduleMidiClipNote(instrumentId: string, clip: Clip, midiNote: MidiNote) {
    const note = `${midiNote.note.letter}${midiNote.note.pitch}`;
    const start = this.toSeconds(clip.start) + this.toSeconds(midiNote.start);
    const duration = start + this.toSeconds(midiNote.duration);
    const trigger = this.getInstrumentTrigger(instrumentId, note, duration);

    this.scheduleEvent(midiNote.id, start, trigger);
  }

  private scheduleEvent(id: string, time: number, callback: (...args: any[]) => void) {
    if (this.scheduled[id]) this.cancelEvent(id);

    const eventId = Tone.Transport.schedule(callback, time);
    this.scheduled[id] = eventId;

    return eventId;
  }

  private cancelEvent(id: string) {
    Tone.Transport.cancel(this.scheduled[id]);
  }

  private toSeconds(value: Tone.Encoding.Time): number {
    return new Tone.Time(value).toSeconds();
  }

  private getInstrumentTrigger(id: string, note: string, duration: number) {
    const instrument = this.store.getInstrument(id);

    return (time: number) =>
      instrument.node.triggerAttackRelease(note, duration, time);
  }
}
