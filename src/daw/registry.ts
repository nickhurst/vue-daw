import { EffectSchema, factoryBuilder as effectsBuilder } from './schema/effect-schema';
import { InstrumentSchema, factoryBuilder as instrumentsBuilder } from './schema/instrument-schema';

export interface FactoryMetadata {
  [key: string]: any;
}

export interface Factory<T = any, M extends FactoryMetadata = FactoryMetadata> {
  ctor: Constructor<T>;
  defaultOptions: { [key: string]: any };
  createInstance(...args: any[]): T;
  metadata?: M;
}

export type FactoryBuilder<S = any, F extends Factory = any> = (schema: S) => F;

interface Index<T = any, M extends FactoryMetadata = FactoryMetadata> {
  [key: string]: Factory<T, M>;
}

class RegistryGroup<S> {
  private readonly index: Map<string, Factory>;

  constructor(private builder: FactoryBuilder<S>) {
    this.index = new Map();
  }

  get(key: string) {
    return this.index.get(key);
  }

  getDefaultConfig(key: string) {
    const factory = this.get(key);
    if (factory) return factory.defaultOptions;
  }

  registerFactory<T, M extends FactoryMetadata = any>(key: string, schema: S): Factory<T, M> {
    const factory = this.builder(schema);
    this.index.set(key, factory);

    return factory;
  }
}

export class Registry {
  readonly effects: RegistryGroup<EffectSchema>;
  readonly instruments: RegistryGroup<InstrumentSchema>;

  constructor() {
    this.effects = new RegistryGroup<EffectSchema>(effectsBuilder);
    this.instruments = new RegistryGroup<InstrumentSchema>(instrumentsBuilder);
  }
}
