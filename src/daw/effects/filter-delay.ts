import Tone from 'tone';
import { Node, NodeMetaData } from '../node';
import { Store } from '../store';

export class FilterDelay extends Node {
  static meta: any = {
    name: 'filter delay',
    type: 'effect',
    members: {
      leftEnabled: {
        name: 'leftEnabled'
      }
    }
  }

  protected leftInputEnabled: boolean;
  protected leftFilterEnabled: boolean;
  protected leftFilterFrequency: Tone.Encoding.Frequency;
  protected leftFilterBandwidth: number;
  protected leftDelayTime: Tone.Encoding.Time;
  protected leftDelayFeedback: Tone.Encoding.NormalRange;
  protected leftGain: Tone.Encoding.Gain;

  protected rightInputEnabled: boolean;
  protected rightFilterEnabled: boolean;
  protected rightFilterFrequency: Tone.Encoding.Frequency;
  protected rightFilterBandwidth: number;
  protected rightDelayTime: Tone.Encoding.Time;
  protected rightDelayFeedback: Tone.Encoding.NormalRange;
  protected rightGain: Tone.Encoding.Gain;

  protected stereoInputEnabled: boolean;
  protected stereoFilterEnabled: boolean;
  protected stereoFilterFrequency: Tone.Encoding.Frequency;
  protected stereoFilterBandwidth: number;
  protected stereoDelayTime: Tone.Encoding.Time;
  protected stereoDelayFeedback: Tone.Encoding.NormalRange;
  protected stereoGain: Tone.Encoding.Gain;

  constructor(id: string, store: Store) {
    super(id, FilterDelay.meta, store);
  }

  init() {
    const node = new Tone.FeedbackDelay() as Tone.AudioNode;

    return node;
  }
}
