import { Daw } from '@/daw/daw';
import { PluginFunction } from 'vue';

function dawInit() {
  const options = this.$options;

  if (options.daw) {
    this.$daw = options.daw;
  } else if (options.parent && options.parent.$daw) {
    this.$daw = options.parent.$daw;
  }
};

const plugin: PluginFunction<undefined> = Vue => {
  Vue.mixin({ beforeCreate: dawInit });
};

export default plugin;
