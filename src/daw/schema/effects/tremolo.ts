import Tone from 'tone';

interface TremoloOptions {
  frequency: Tone.Encoding.Frequency;
  type: Tone.OscillatorType;
  depth: Tone.Encoding.NormalRange;
  spread: Tone.Encoding.Degrees;
}

export const tremolo = {
  effect: Tone.Tremolo,
  options: {
    defaults: {
      frequency: 10,
      type: 'sine',
      depth: 0.5,
      spread: 180,
    },
  },
  create({ frequency, depth }: TremoloOptions) {
    return new Tone.Tremolo(frequency, depth).start()
  },
  signalProps: [
    'depth',
    'frequency',
    'wet',
  ],
};
