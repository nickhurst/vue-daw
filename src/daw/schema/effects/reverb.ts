import Tone from 'tone';

interface ReverbOptions {
  decay: number;
  preDelay: number;
}

export const reverb = {
  effect: Tone.Reverb,
  options: {
    defaults: {
      decay: 1.5,
      preDelay: 0.01,
      wet: 1,
    },
    transform({ decay }: ReverbOptions) {
      return { decay };
    },
  },
  async create({ decay }: ReverbOptions) {
    const effect = new Tone.Reverb(decay);
    return await effect.generate();
  },
  signalProps: ['wet'],
}

