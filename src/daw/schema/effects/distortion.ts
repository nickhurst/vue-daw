import Tone from 'tone';

interface DistortionOptions {
  distortion?: Tone.Encoding.NormalRange;
  oversample?: 'none' | '2x' | '4x';
}

export const distortion = {
  effect: Tone.Distortion,
  options: {
    defaults: {
      distortion: 0.4,
      oversample: 'none',
    },
  },
  create({ distortion, oversample }: DistortionOptions) {
    return new Tone.Distortion({ distortion, oversample });
  },
  signalProps: [
    'wet',
  ],
};
