import Tone from 'tone';

interface FeedbackDelayOptions {
  delayTime?: Tone.Encoding.Time;
  feedback?: number;
}

export const feedbackDelay = {
  effect: Tone.FeedbackDelay,
  options: {
    defaults: {
      delayTime: 0.25,
      feedback: 0.125,
    },
  },
  create({ delayTime, feedback }: FeedbackDelayOptions) {
    return new Tone.FeedbackDelay(delayTime, feedback);
  },
  signalProps: [
    'delayTime',
    'wet',
    'feedback',
  ],
};
