import { distortion } from './distortion';
import { feedbackDelay } from './feedback-delay';
import { reverb } from './reverb';
import { tremolo } from './tremolo';

export const effectSchemas = {
  distortion,
  feedbackDelay,
  reverb,
  tremolo,
};
