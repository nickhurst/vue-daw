import { defaultTo, identity, mergeDeepRight } from 'ramda';
import { Factory, FactoryBuilder, FactoryMetadata } from '../registry';

export interface EffectOptionsSchema {
  defaults?: { [key: string]: any };
  transform?: (options: object) => object;
}

export interface EffectSchema {
  effect: Constructor<Tone.ProcessingNode>;
  options?: EffectOptionsSchema;
  create?: (options: object) => Tone.ProcessingNode | Promise<Tone.ProcessingNode>;
  signalProps?: string[];
}

export interface EffectMetadata extends FactoryMetadata {
  signals: string[];
}

export interface EffectFactory extends Factory<Tone.ProcessingNode, EffectMetadata> {}

export const factoryBuilder: FactoryBuilder<EffectSchema, EffectFactory> =
  ({ effect, options, create, signalProps }) => ({
    ctor: effect,
    defaultOptions: defaultTo({}, options.defaults),
    createInstance(opts = {}) {
      const withDefaults = mergeDeepRight(defaultTo({}, options.defaults), opts);
      const transformer = defaultTo(identity, options.transform) as EffectOptionsSchema['transform'];
      const transformed = transformer(withDefaults);

      return (create ? create(transformed) : new effect(transformed)) as Tone.ProcessingNode;
    },
    metadata: {
      signals: signalProps,
    },
  });
