import Tone from 'tone';
import { mergeDeepRight } from 'ramda';

interface EnvelopeOptions {
  attack?: number;
  decay?: number;
  sustain?: number;
  release?: number;
}

interface OscillatorOptions {
  frequency?: number;
  detune?: number;
  type?: Tone.OscillatorType;
  release?: number;
  phase?: number;
}

interface SynthOptions {
  detune?: number;
  frequency?: number;
  oscillator?: OscillatorOptions;
  envelope?: EnvelopeOptions;
  volume?: number;
  portamento?: number;
}

const ENVELOPE_DEFAULTS = {
  attack: 0.005,
  decay: 0.1,
  sustain: 0.3,
  release: 1,
};

const OSCILLATOR_DEFAULTS: OscillatorOptions = {
  frequency: 440,
  detune: 0,
  type: 'sine',
  phase: 0,
};

export const synth = {
  instrument: Tone.Synth,
  options: {
    defaults: {
      volume: 0,
      portamento: 0,
      detune: OSCILLATOR_DEFAULTS.detune,
      frequency: OSCILLATOR_DEFAULTS.frequency,
      envelope: ENVELOPE_DEFAULTS,
      oscillator: OSCILLATOR_DEFAULTS,
    },
    transformer: ({ detune, frequency, envelope, oscillator }: SynthOptions) => ({
      ...envelope,
      oscillator: mergeDeepRight(oscillator, { detune, frequency }),
    }),
  },
  signalProps: [
    'detune',
    'frequency',
    'volume',
  ],
  create: {
    mono(options: SynthOptions) {
      return new Tone.Synth(options);
    },
    poly(options: SynthOptions, voices = 4) {
      const synth = new Tone.PolySynth(voices, Tone.Synth);
      synth.set(options);

      return synth;
    },
  },
};
