import Tone from 'tone';
import { defaultTo, identity, mergeDeepRight } from 'ramda';
import { Factory, FactoryBuilder, FactoryMetadata } from '../registry';
import { isObject } from '@/utils/predicates';

export interface InstrumentOptionsSchema {
  defaults?: { [key: string]: any };
  transform?: (options: object) => object;
}

export type InstrumentCreateFunction<T extends Tone.Instrument = any> = (...args: any[]) => T;

export interface InstrumentCreateObject<T extends Tone.Instrument = any> {
  mono: InstrumentCreateFunction<T>;
  poly?: InstrumentCreateFunction<Tone.PolySynth>;
}

export interface InstrumentSchema {
  instrument: Constructor<Tone.Instrument>;
  options?: InstrumentOptionsSchema;
  create?: InstrumentCreateFunction | InstrumentCreateObject;
  signalProps?: string[];
}

export interface InstrumentMetadata extends FactoryMetadata {
  signals: string[];
}

export interface InstrumentFactory extends Factory<Tone.Instrument, InstrumentMetadata> {}

export const factoryBuilder: FactoryBuilder<InstrumentSchema, InstrumentFactory> =
  ({ instrument, options, create, signalProps }) => ({
    ctor: instrument,
    defaultOptions: defaultTo({}, options.defaults),
    createInstance(opts = {}, { poly = false } = {}) {
      const withDefaults = mergeDeepRight(defaultTo({}, options.defaults), opts);
      const transformer = defaultTo(identity, options.transform) as InstrumentOptionsSchema['transform'];
      const transformed = transformer(withDefaults);

      if (isObject(create)) {
        const { mono: monoCreate, poly: polyCreate } = create as InstrumentCreateObject;
        const createFn = poly && polyCreate ? polyCreate : monoCreate;

        return createFn(transformed);
      }

      const createFn = (create || new instrument(transformed)) as InstrumentCreateFunction;
      return createFn(transformed);
    },
    metadata: {
      signals: signalProps,
    },
  })

// const DEFAULT_INSTRUMENT_ATTRS = [];
// const DEFAULT_INSTRUMENT_SIGNALS = ['volume'];

// const DEFAULT_PROPERTIES = {
//   attrs: DEFAULT_INSTRUMENT_ATTRS,
//   signals: DEFAULT_INSTRUMENT_SIGNALS,
// };

// type OptionsTransformer<O> = (options: Partial<O>) => Partial<O>;

// interface InstrumentOptionsSchema<O> {
//   defaults?: Partial<O>;
//   transformer?: OptionsTransformer<O>;
// }

// interface InstrumentPropertiesSchema {
//   attrs?: string[];
//   signals?: string[];
// }

// interface SchemaOptions<T, O> {
//   instrument: Constructor<T, [O]>;
//   options?: InstrumentOptionsSchema<O>;
//   properties?: InstrumentPropertiesSchema;
// }

// export interface Schema<T, O> {
//   instrument: Constructor<T, [O]>;
//   properties?: InstrumentPropertiesSchema;
//   defaultOptions?: Partial<O>;
//   buildOptions: (options?: Partial<O>) => Partial<O>;
// };

// export const buildSchema =
//   <T extends Tone.Instrument, O = {}>(schema: SchemaOptions<T, O>): Schema<T, O> => {
//     const defaults = defaultTo({}, schema.options.defaults);
//     const instrument = schema.instrument;
//     const properties = mergeDeepRight(DEFAULT_PROPERTIES, schema.properties)

//     const buildOptions = (options: object): any => {
//       const defaulted = mergeDeepRight(defaults, options);
//       const transformer = defaultTo(identity, schema.options.transformer) as any;
//       const transformed = transformer(defaulted);

//       return transformed;
//     };

//     return { buildOptions, instrument, properties };
//   };
