import { Master } from './master';
import { Scheduler } from './scheduler';
import { Store } from './store';
import { Transport } from './transport';
import { EffectSchema, factoryBuilder as effectsBuilder } from './schema/effect-schema';
import { effectSchemas } from './schema/effects';
import { instrumentSchemas } from './schema/instruments';
import { Registry } from './registry';
import { Effect } from '@/models/effect';
import { Instrument } from '@/models/instrument';
import { Track } from '@/models/track';

export class Daw {
  readonly master: Master;
  readonly transport: Transport;
  readonly scheduler: Scheduler;
  readonly store: Store;
  readonly registry: Registry;

  constructor() {
    this.store = new Store();
    this.master = new Master(this.store);
    this.transport = new Transport();
    this.scheduler = new Scheduler(this.store);
    this.registry = new Registry();

    Object.keys(effectSchemas).forEach(name => {
      const schema = effectSchemas[name];
      this.registry.effects.registerFactory(name, schema);
    });

    Object.keys(instrumentSchemas).forEach(name => {
      const schema = instrumentSchemas[name];
      this.registry.instruments.registerFactory(name, schema);
    });
  }

  createEffect(config: any): Effect {
    const effect = new Effect(this.store, this.registry, config);
    this.store.addEffect(effect);

    return effect;
  }

  createTrack(config: any): Track {
    const track = new Track(this.store, this.registry, config);
    this.store.addTrack(track);

    return track;
  }

  createInstrument(config: any): Instrument {
    const instrument = new Instrument(this.store, this.registry, config);
    this.store.addInstrument(instrument);

    return instrument;
  }
}
