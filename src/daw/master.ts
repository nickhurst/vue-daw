import Tone from 'tone';
import { Effect } from '@/models/effect';
import { Store } from './store';

export class Master {
  private _connections: string[];
  private _volume: Tone.Volume;
  private readonly store: Store;
  readonly node: Tone.Master;

  constructor(store: Store) {
    this.node = Tone.Master;
    this.store = store;

    this._connections = [];
    this._volume = new Tone.Volume({ volume: 0, mute: false });

    this.buildConnectionChain();
  }

  get connections(): ReadonlyArray<Effect> {
    return this._connections.map(id => this.store.getEffect(id));
  }

  get volume() {
    return this._volume.volume.value;
  }

  set volume(value: number) {
    this._volume.volume.value = value;
  }

  private buildConnectionChain(): void {
    const nodes = this.connections.map(conn => conn.node);
    this.node.chain(...nodes, this._volume);
  }
}
