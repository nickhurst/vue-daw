import daw from './index';
import Tone from 'tone';
import { Store } from './store';
import { isPromise } from '@/utils/predicates';

export type NodeType = 'instrument' | 'effect';

export interface NodeProperty<T = any> {
  name: string;
  signal: boolean;
  getter?: (node: Node) =>  T;
  label?: string;
  desc?: string;
}

export interface NodeMetaData {
  name: string;
  type: NodeType;
  members: { [key: string]: NodeProperty };
}

export abstract class Node {
  public readonly id: string;
  protected _node: Tone.AudioNode;
  protected readonly meta: NodeMetaData;
  protected readonly store: Store;
  protected _initialized: boolean;

  constructor(id: string, meta: NodeMetaData, store: Store) {
    this.id = id;
    this.meta = meta;
    this.store = store;
    this._initialized = false;

    this._init(this.init())
  }

  abstract init(): Tone.AudioNode | Promise<Tone.AudioNode>

  get node() {
    return this._node;
  }

  get master() {
    return daw.master.node;
  }

  protected destroy() {
    this.node.dispose();
    this._initialized = false;
  }

  private async _init(node: Tone.AudioNode | Promise<Tone.AudioNode>) {
    if (isPromise(node)) {
      this._node = await (node as Promise<Tone.AudioNode>);
    } else {
      this._node = node as Tone.AudioNode;
    }

    this._initialized = true;
  }
}
