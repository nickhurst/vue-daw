import Tone from 'tone';
import { init, last } from 'ramda';
import { Store } from './store';
import daw from './index';

export class DAWObject {
  constructor(protected readonly store: Store, readonly id: string) {}

  set(keyPath: string, value: any): void {
    const [target, key] = this.parseKeyPath(keyPath);

    target[key] = value;
  }

  protected getMaster(): Tone.Master {
    return daw.master.node;
  }

  protected getSignalValue(target: any, key: string) {
    return target[key].value;
  }

  protected setSignalValue(target: any, key: string, value: any) {
    target[key].value = value;
  }

  protected parseKeyPath(keyPath: string, initialTarget: any = this): [any, string] {
    const path = keyPath.split('.');
    const target = this.getKeyPathTarget(init(path), initialTarget);
    const prop = last(path);

    return [target, prop];
  }

  protected getKeyPathTarget(path: string[], intialTarget: any): any {
    return path.reduce((target, prop) => target[prop], intialTarget);
  }
}
