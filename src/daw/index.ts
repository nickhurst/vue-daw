import Vue from 'vue';

import { Daw } from './daw';
import DawPlugin from './plugin';

Vue.use(DawPlugin);

export default new Daw();
