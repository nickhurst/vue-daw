// import { Daw } from '../daw/daw';

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

type Func<T extends any[] = any[], R = any> = (...args: T) => R;
type CTor<T, A extends Func = Func> = { new() }
interface Ctor<T, A extends Func = Func> {
  new(...args: Parameters<A>): T;
}

type Constructor<T, A extends any[] = any[]> = {
  new (...args: A): T;
}

type Notation = '1n' | '2n' | '4n' | '8n' | '36n';

type WrappedFunction<T extends (...args: any[]) => any, R = unknown> =
  (...args: Parameters<T>) => ReturnType<T> | void;

type GetterFunctions<T, S, R> = {
  [K in keyof T]: (state: S, getters: T, rootState: R, rootGetters: any) => T[K];
};

type TypedArrayContructor = Uint8ArrayConstructor
  | Int8ArrayConstructor
  | Uint16ArrayConstructor
  | Int16ArrayConstructor
  | Uint32ArrayConstructor
  | Int32ArrayConstructor
  | Float32ArrayConstructor
  | Float64ArrayConstructor;

type RGB = [number, number, number];
interface RGBObject { rgb: number[] }

type XYCoordPair = [number, number];
type XYCoordGenerator = (seed: number) => XYCoordPair;
interface XYCoordObject { x: number, y: number }

declare namespace Store {
  interface KeyValuePayload<T, K extends keyof T> {
    key: K;
    value: T[K];
  }

  interface UpdateIndexPayload<T> extends KeyValuePayload<T, keyof T> {
    id: number;
  }

  interface Index<T, S extends string | number = string> {
    index: { [key: string]: T; [key: number]: T };
  }

  interface IndexWithSequence<T> extends Index<T> {
    keySeq: number;
  }
}
