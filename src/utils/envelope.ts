import { flatten, memoize, pipe, values as objValues, zipObj } from 'ramda';

import { ADSR } from './constants/adsr';
import { logScale } from './functions';

interface EnvelopeBuilderOptions {
  value: number;
  start: {
    x: number;
    y: number;
    [key: string]: number;
  };
  delta: {
    x: (start: number, value: number, opts: object) => number;
    y: (start: number, value: number, opts: object) => number;
    [key: string]: (start: number, value: number, opts: object) => number;
  };
  curve?: {
    type?: string;
    scaled?: {
      values: {
        min: (start: { x: number; y: number }, value: number) => number;
        max: (start: { x: number; y: number }, value: number) => number;
        [key: string]: (start: { x: number; y: number }, value: number) => number;
      };
      points?: {
        min: number;
        max: number;
        [key: string]: number;
      };
    };
  };
}

interface Envelope {
  points: number[];
  start: [number, number];
  c1?: [number, number];
  c2?: [number, number];
  end: [number, number];
}

const toXYObject: (pair: [number, number]) => { x: number; y: number } = ([x, y]) => ({ x, y });
const toPointArr: (curve: any) => number[] = pipe(objValues, flatten);

const calcExpControlPoints = (start, x, y) => {
  const c = [start * (1 ** x), y];
  return { c1: c, c2: c };
};

interface CalcScaledValueOpts {
  values: { [key: string]: (start: { x: number; y: number }, value: number) => number; };
  points?: { [key: string]: number; };
  start: {
    x: number;
    y: number;
    [key: string]: number;
  }
  envelopeVal: number;
}

const calcScaledValue: (opts: CalcScaledValueOpts) => number =
  ({ values, points, start, envelopeVal }) => {
    const [scaleMin, scaleMax] = objValues(values).map(fn => fn(start, envelopeVal));
    return logScale(points.min, points.max, scaleMin, scaleMax, envelopeVal);
  };

export const envelopeBuilder = {
  build: ({ start, value: envelopeVal, delta, curve: opts = {} }: EnvelopeBuilderOptions): Envelope => {
    const value = opts && opts.scaled
      ? calcScaledValue({ ...opts.scaled, start, envelopeVal })
      : envelopeVal;
    const end = ['x', 'y'].map(ax => delta[ax](start[ax], value, opts)) as [number, number];
    const controls = opts && opts.type && opts.type === 'exponential'
      ? calcExpControlPoints(start.x, value, end[1]) : {};

    const curve = { start: (objValues(start) as [number, number]), ...controls, end };
    return { points: toPointArr(curve), ...curve };
  },
};

export const buildADSREnvelope = (opts = {}, height = 100) => {
  const adsr = { ...ADSR.DEFAULTS.ENVELOPE, ...opts };
  const sustainHeight = height - (adsr.sustain * height);

  const attackEnvelope = envelopeBuilder.build({
    value: adsr.attack,
    start: toXYObject([0, height]),
    delta: {
      x: (start, value) => start + value,
      y: start => start - height,
    },
    curve: {
      type: adsr.attackCurve,
      scaled: {
        values: {
          min: () => 1,
          max: () => 100,
        },
        points: {
          min: ADSR.ATTACK.MIN,
          max: ADSR.ATTACK.MAX,
        },
      },
    },
  });

  const decayEnvelope = envelopeBuilder.build({
    value: adsr.decay,
    start: toXYObject(attackEnvelope.end),
    delta: {
      x: (start, value) => start + value,
      y: () => sustainHeight,
    },
    curve: {
      type: 'linear',
      scaled: {
        values: {
          min: start => start.x,
          max: () => 200,
        },
        points: {
          min: ADSR.DECAY.MIN,
          max: ADSR.DECAY.MAX,
        },
      },
    },
  });

  const sustainEnvelope = envelopeBuilder.build({
    value: adsr.sustain,
    start: toXYObject(decayEnvelope.end),
    delta: {
      x: start => start + Math.max(200 - start, 0),
      y: start => start,
    },
  });

  const releaseEnvelope = envelopeBuilder.build({
    value: adsr.release,
    start: toXYObject(sustainEnvelope.end),
    delta: {
      x: (start, value) => start + value,
      y: start => start + (height - start),
    },
    curve: {
      type: adsr.releaseCurve,
      scaled: {
        values: {
          min: start => start.x - 175,
          max: () => 120,
        },
        points: {
          min: ADSR.RELEASE.MIN,
          max: ADSR.RELEASE.MAX,
        },
      },
    },
  });

  return {
    attack: {
      points: attackEnvelope.points,
      bezier: adsr.attackCurve === 'exponential',
    },
    decay: {
      points: decayEnvelope.points,
    },
    sustain: {
      points: sustainEnvelope.points,
    },
    release: {
      points: releaseEnvelope.points,
      bezier: adsr.releaseCurve === 'exponential',
    },
  };
};
