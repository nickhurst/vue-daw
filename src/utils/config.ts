import { UnparsedColorConfig } from './color';
import COLOR_CONFIG from '@/config/colors';

export interface Config {
  color: UnparsedColorConfig;
}

export const config: Config = {
  color: COLOR_CONFIG,
};
