export enum WheelDirection {
  Up = 1,
  Down = -1,
  Left = 1,
  Right = -1,
};

export const wheelEventXDirection = (e: WheelEvent) =>
  e.deltaX < 0 ? WheelDirection.Left : WheelDirection.Right;

export const wheelEventYDirection = (e: WheelEvent) =>
  e.deltaY < 0 ? WheelDirection.Up : WheelDirection.Down;
