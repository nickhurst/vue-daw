import { createCanvasXYPointArray } from './functions';

type Waves = 'sine' | 'square' | 'triangle' | 'sawtooth';
type GeneratorBuilder = (height: number, width: number) => XYCoordGenerator;

const sine: GeneratorBuilder = height => {
  const amp = height / 4;
  const freq = 6 / height;
  const offset = height / 2;

  return x => {
    const y = (amp * Math.sin(freq * x)) + offset;
    return [x, y];
  };
};

const square: GeneratorBuilder = (height, width) => {
  const top = height / 2;
  const period = width / 4;
  const offset = height / 4;

  return x => {
    const y = (((x + 1) % period) < top ? top : 0) + offset;
    return [x, y];
  };
};

const triangle: GeneratorBuilder = (height, width) => {
  const top = height / 2;
  const period = width / 8;
  const offset = height / 4;

  return x => {
    const y = ((top / period) * (period - Math.abs((x % (2 * period)) - period))) + offset;
    return [x, y];
  };
};

const sawtooth: GeneratorBuilder = height => {
  const top = height / 2;
  const offset = height / 4;

  return x => {
    const y = ((x + 1) % top) + offset;
    return [x, y];
  };
};

const generators: { [key: string]: GeneratorBuilder } = {
  sine, square, triangle, sawtooth,
};

export const generateWave: (wave: Waves, height: number, width: number) => ArrayLike<number> =
  (wave, height, width) => {
    const generator = generators[wave](height, width);
    return createCanvasXYPointArray(width, generator);
  };
