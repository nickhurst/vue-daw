import { isObject } from './predicates';
import { config } from './config';

export type RGB = [number, number, number];

export type Shade = 'light' | 'lighter' | 'dark' | 'darker';

export type ColorShades = Record<Shade | 'base', RGB>;

export interface ColorGeneratorOptions {
  opacity?: number;
}

export interface ColorGenerator {
  (): string;
  (options: ColorGeneratorOptions): string;
  hex(): string;
  opacity(amount?: number): string;
}

export interface ColorShadeGeneratorOptions extends ColorGeneratorOptions {
  shade?: Shade | 'base';
}

export interface ColorShadeGenerator extends ColorGenerator {
  (options: ColorShadeGeneratorOptions): string;
  hex(options?: Omit<ColorShadeGeneratorOptions, 'opacity'>): string;
  light: ColorGenerator;
  lighter: ColorGenerator;
  dark: ColorGenerator;
  darker: ColorGenerator;
}

export interface ColorPalette {
  black: ColorShades;
  white: ColorShades;
  grey: ColorShades;
  blue: ColorShades;
  red: ColorShades;
  green: ColorShades;
  yellow: ColorShades;
  purple: ColorShades;
}

export type ColorPaletteGenerators = Record<keyof ColorPalette, ColorShadeGenerator>

export interface ThemeColors {
  primary: RGB;
  success: RGB;
  warning: RGB;
  danger: RGB;
  info: RGB;
}

export type ThemeColorGenerators = Record<keyof ThemeColors, ColorGenerator>;

export type ColorPaletteConfig<T = string> = Record<keyof ColorPalette, Record<Shade | 'base', T>>;
export type ColorThemeConfig<T = string> = Record<keyof ThemeColors, T>;

export interface UnparsedColorConfig {
  palette: ColorPaletteConfig;
  theme: ColorThemeConfig;
}

export interface ColorConfig {
  palette: ColorPaletteConfig<RGB>;
  theme: ColorThemeConfig<RGB>;
}

const parseRGB: (str: string) => RGB = str => str
  .replace(/rgb(a)?\(/, '')
  .replace(/\)$/, '')
  .split(', ')
  .slice(0, 3)
  .map(c => parseFloat(c)) as RGB;

type ParsedObject<T, V> = {
  [K in keyof T]: T[K] extends { [k: string]: string | object } ? ParsedObject<T[K], V> :
                  T[K] extends string ? V :
                  never;
}

export const parseRGBValues = <T>(obj: T): ParsedObject<T, RGB> =>
  Object.keys(obj).reduce((parsed , key) => {
    const raw = obj[key];
    const value = isObject(raw)
      ? parseRGBValues(raw)
      : parseRGB(raw);

    return { ...parsed, [key]: value };
  }, <any>{});


export const parseConfig = ({ palette, theme }: UnparsedColorConfig): ColorConfig => ({
  palette: parseRGBValues(palette),
  theme: parseRGBValues(theme),
});

export const toHex = (color: RGB) =>
  `#${color.map(n => n.toString(16)).join('')}`;
export const toRGB = (color: RGB) =>
  `rgb(${color.join(', ')})`;
export const toRGBA = (color: RGB, opacity: number = 1.0) =>
  `rgba(${color.join(', ')}, ${(opacity).toFixed(1)})`;

const buildColorGenerator: (color: RGB) => ColorGenerator = color => {
  const generator = ({ opacity }: ColorGeneratorOptions = {}) =>
    toRGBA(color, opacity);

  generator.hex = () => toHex(color);
  generator.opacity = (amount = 1) => toRGBA(color, amount);

  return generator as ColorGenerator;
}

const buildColorShadeGenerator: (shades: ColorShades) => ColorShadeGenerator =
  shades => {
    const generator = ({ opacity, shade }: ColorShadeGeneratorOptions = {}) =>
      toRGBA(shades[shade || 'base'], opacity);

    generator.hex = ({ shade }: Omit<ColorShadeGeneratorOptions, 'opacity'> = {}) =>
      toHex(shades[shade || 'base']);
    generator.opacity = (amount = 1) => toRGBA(shades.base, amount);

    generator.light = buildColorGenerator(shades.light);
    generator.lighter = buildColorGenerator(shades.lighter);
    generator.dark = buildColorGenerator(shades.dark);
    generator.darker = buildColorGenerator(shades.darker);

    return generator as ColorShadeGenerator;
  };

const buildColorPaletteGenerators: (palette: ColorPalette) => ColorPaletteGenerators =
  palette => ({
    black: buildColorShadeGenerator(palette.black),
    white: buildColorShadeGenerator(palette.white),
    grey: buildColorShadeGenerator(palette.grey),
    blue: buildColorShadeGenerator(palette.blue),
    red: buildColorShadeGenerator(palette.red),
    green: buildColorShadeGenerator(palette.green),
    yellow: buildColorShadeGenerator(palette.yellow),
    purple: buildColorShadeGenerator(palette.purple),
  });

const buildThemeColorGenerators: (theme: ThemeColors) => ThemeColorGenerators =
  theme => ({
    primary: buildColorGenerator(theme.primary),
    success: buildColorGenerator(theme.success),
    warning: buildColorGenerator(theme.warning),
    danger: buildColorGenerator(theme.danger),
    info: buildColorGenerator(theme.info),
  });

interface Color extends ColorPaletteGenerators, ThemeColorGenerators {}

const { palette, theme } = parseConfig(config.color);
const paletteGenerators = buildColorPaletteGenerators(palette);
const themeGenerators = buildThemeColorGenerators(theme);

export const color: Color = {
  ...paletteGenerators,
  ...themeGenerators,
}

window['color'] = color;
