import { test } from 'ramda';
import Tone from 'tone';

export const isSharp = test(/[A-G]#$/);

export const synth = Tone.Synth;
let sy = new Tone.Synth();
sy.triggerAttackRelease;