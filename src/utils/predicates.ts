import { equals, pipe, type } from 'ramda';

export const isObject: (val: any) => boolean = pipe(type, equals('Object'));
export const isPromise: (val: any) => boolean = pipe(type, equals('Promise'));
