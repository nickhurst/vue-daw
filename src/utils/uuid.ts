import { randomNum } from './number';

/**
 * Type assertions in the template exist because this is essentially
 * abusing JavaScript's type coercian to create the template string
 * @see https://stackoverflow.com/a/2117523
 */
const UUID_TEMPLATE = <string><unknown>(<number><unknown>[1e7]+-1e3+-4e3+-8e3+-1e11);

export const uuid: () => string = () =>
  UUID_TEMPLATE.replace(/[018]/g, c => {
    const num = parseInt(c);
    return (num ^ randomNum() & 15 >> num / 4).toString(16);
  });
