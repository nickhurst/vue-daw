export const logScale: (
  pMin:number,
  pMax: number,
  vMin: number,
  vMax: number,
  value: number
) => number =
  (pMin, pMax, vMin, vMax, value) => {
    [vMin, vMax] = [vMin || 1e-4, vMax || 1e-4].map(v => Math.log(v));
    const scale = (vMax - vMin) / (pMax - pMin);

    return Math.exp(vMin + (scale * (value - pMin)));
  };

export const createCanvasXYPointArray: (
  width: number,
  generator:
  XYCoordGenerator,
  arrayCtor?: TypedArrayContructor
) => ArrayLike<number> =
  (width, generator, ArrayType = Float32Array) => {
    const length = width * 2;
    const buffer = new ArrayBuffer(length * 4);
    const floatArr = new ArrayType(buffer);

    let seed = 0;
    for (let i = 0; i < length; i += 2) {
      const [x, y] = generator(seed);
      floatArr[i] = x;
      floatArr[i + 1] = y;
      seed++;
    }

    return floatArr;
  };

type Fn = (...args: any[]) => any;

export const debounce =
  <T extends Fn = Fn>(delay: number, func: T): WrappedFunction<T, void> => {
    let timerId: NodeJS.Timeout;
    return (...args) => {
      if (timerId) clearTimeout(timerId);

      timerId = setTimeout(() => {
        func(...args);
        timerId = null;
      }, delay);
    };
  };

export const throttle =
  <T extends Fn = Fn>(delay: number, func: T): WrappedFunction<T> => {
    let lastCall = 0;
    return (...args) => {
      const now = new Date().getTime();
      if (now - lastCall < delay) return;

      lastCall = now;
      return func(...args);
    };
  };

export const memoize = <T extends Fn>(func: T): WrappedFunction<T> => {
  const cache = new Map();
  const memoized = function(...args: Parameters<T>) {
    if (cache.has(args)) return cache.get(args);

    const value = func.call(this, ...args);
    cache.set(args, value);

    return value;
  }

  memoized['cache'] = cache;
  return memoized;
}
