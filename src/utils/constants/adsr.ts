import { color } from '../color';

const ADSR_DEFAULTS = {
  ATTACK: {
    MIN: 0,
    MAX: 5,
    DEFAULT: 0.1,
    CURVE: 'linear',
  },
  DECAY: {
    MIN: 0,
    MAX: 20,
    DEFAULT: 0.5,
  },
  SUSTAIN: {
    MIN: 0,
    MAX: 1,
    DEFAULT: 0.5,
  },
  RELEASE: {
    MIN: 0,
    MAX: 20,
    DEFAULT: 1,
    CURVE: 'exponential',
  },
  GRAPH: {
    DEFAULTS: { HEIGHT: 100, WIDTH: 325 },
    CURVE: {
      DEFAULTS: {
        stroke: color.yellow.lighter(),
        strokeWidth: 3,
        lineCap: 'round',
        lineJoin: 'round',
        shadowForStrokeEnabled: true,
        shadowColor: color.black({ opacity: 0.5, shade: 'light' }),
        shadowOffset: { x: 2, y: 4 },
      },
    },
  },
};

export const ADSR = {
  DEFAULTS: {
    ENVELOPE: {
      attack: ADSR_DEFAULTS.ATTACK.DEFAULT,
      attackCurve: ADSR_DEFAULTS.ATTACK.CURVE,
      decay: ADSR_DEFAULTS.DECAY.DEFAULT,
      sustain: ADSR_DEFAULTS.SUSTAIN.DEFAULT,
      release: ADSR_DEFAULTS.RELEASE.DEFAULT,
      releaseCurve: ADSR_DEFAULTS.RELEASE.CURVE,
    },
  },
  ...ADSR_DEFAULTS,
};

export type ADSRDefaults = {
  [K in keyof typeof ADSR]: any;
}
