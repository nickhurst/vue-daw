interface ColorDefinition extends RGBObject {
  alt?: RGBObject;
}
interface ColorDefinitionShade {
  light?: ColorDefinition;
  dark?: ColorDefinition
};

export interface ColorObject extends ColorDefinition, ColorDefinitionShade {}

export const COLOR = {
  BLACK: {
    rgb: [32, 37, 41],
    alt: {
      rgb: [50, 56, 61],
    },
  },
  WHITE: {
    rgb: [245, 246, 250],
    alt: {
      rgb: [220, 221, 225],
    },
  },
  GREY: {
    rgb: [87, 97, 105],
    alt: {
      rgb: [76, 85, 92],
    },
    light: {
      rgb: [97, 109, 117],
      alt: {
        rgb: [108, 120, 130],
      },
    },
  },
  BLUE: {
    rgb: [72, 126, 176],
    alt: {
      rgb: [116, 125, 140],
    },
    light: {
      rgb: [0, 168, 255],
      alt: {
        rgb: [0, 151, 230],
      },
    },
    dark: {
      rgb: [39, 60, 117],
      alt: {
        rgb: [25, 42, 86],
      },
    },
  },
  RED: {
    rgb: [232, 65, 24],
    alt: {
      rgb: [194, 54, 22],
    },
  },
  GREEN: {
    rgb: [76, 209, 55],
    alt: {
      rgb: [68, 189, 50],
    },
  },
  YELLOW: {
    rgb: [251, 197, 49],
    alt: {
      rgb: [225, 177, 44],
    },
  },
  PURPLE: {
    rgb: [156, 136, 255],
    alt: {
      rgb: [140, 122, 230],
    },
  },
};

type AppColors = keyof typeof COLOR;
export type ColorDefinitions = {
  [K in keyof typeof COLOR]: ColorObject
}
