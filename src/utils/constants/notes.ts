import { range, test, xprod, zipObj } from 'ramda';

const NOTE_LETTERS = [
  'C',
  'C#',
  'D',
  'D#',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'A',
  'A#',
  'B',
];

// const NOTE_PITCHES = range(3, 5);
const NOTE_PITCHES = range(1, 7);

const sortNotes = ([noteA, pitchA], [noteB, pitchB]) => {
  if (pitchA !== pitchB) return pitchB - pitchA;
  return NOTE_LETTERS.indexOf(noteB) - NOTE_LETTERS.indexOf(noteA);
};

const toNoteObj = zipObj(['letter', 'pitch']);

export const NOTES = xprod(NOTE_LETTERS, NOTE_PITCHES)
  .sort(sortNotes)
  .map(toNoteObj);
