import { PluginFunction } from 'vue';
import { toPairs } from 'ramda';

import UiButton from './UiButton.vue';
import UiButtonSelect from './UiButtonSelect.vue';
import UiCard from './UiCard.vue';
import UiIcon from './UiIcon.vue';
import UiKnob from './UiKnob.vue';
import UiPanel from './UiPanel.vue';
import UiPowerButton from "./UiPowerButton.vue";

const COMPONENTS = toPairs({
  UiButton,
  UiButtonSelect,
  UiCard,
  UiIcon,
  UiKnob,
  UiPanel,
  UiPowerButton,
});

export const UI: PluginFunction<undefined> = Vue =>
  COMPONENTS.forEach(([name, comp]) => Vue.component(name, comp));

export {
  UiButton,
  UiButtonSelect,
  UiCard,
  UiIcon,
  UiKnob,
  UiPanel,
  UiPowerButton,
};
