import Vue, { PluginObject } from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import VueKonva from 'vue-konva';
import ElementUI from 'element-ui';
import './assets/element-variables.scss';
import './assets/global.scss';

import App from './App.vue';
import daw from './daw';
import router from './router';
import store from './store';

import { UI } from './components/ui';
import { FontAwesome } from './plugins/font-awesome';

import Tone from 'tone';

import './styles/quasar.styl'
import iconSet from 'quasar/icon-set/fontawesome-v5.js'
import '@quasar/extras/fontawesome-v5/fontawesome-v5.css'
import {
  Quasar,
  QLayout,
  QHeader,
  QDrawer,
  QPageContainer,
  QPage,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
} from 'quasar';

Vue.use(Quasar, {
  config: {},
  components: {
    QLayout,
    QHeader,
    QDrawer,
    QPageContainer,
    QPage,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
  },
  directives: {
  },
  plugins: {
  },
  iconSet: iconSet
 })

window['Tone'] = Tone;

window['daw'] = daw;

Vue.use(UI);
Vue.use(ElementUI);
Vue.use(FontAwesome);
Vue.use(VueKonva as PluginObject<any>);

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.config.productionTip = false;

new Vue({
  daw,
  router,
  store,
  render: h => h(App),
} as any).$mount('#app');

